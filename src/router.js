import {
    createWebHistory,
    createRouter
} from 'vue-router';
import Marks from "./components/Marks.vue";
import Evaluation from "./components/Evaluation.vue";
import Class from "./components/Class.vue";
import Criterion from "./components/Criterion.vue";
import EvaluationConfig from "./components/EvaluationConfig.vue";
import PageNotFound from "./components/PageNotFound.vue";

const routes = [
    {path: '/', name: 'home', component: Evaluation}, //Liste des évaluations
    {path: '/add', component: EvaluationConfig}, //Ajouter une évaluation
    {path: '/marks/:id', name: 'marks', component: Marks}, // Notation
    {path: '/criterion', name: 'criterion', component: Criterion}, // Ajouter une liste de critères
    {path: '/criterion/:id', component: Criterion}, // Une liste de critères
    {path: '/classes/:id', name: 'classes', component: Class}, // Une classe d'élèves
    {path: '/:catchAll(.*)*', name: 'pagenotfound', component: PageNotFound},
]

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;