export default class Test {
    id = null;
    name = "";

    constructor(value) {
        this.id = value.id ?? null;
        this.name = value.name;
    }

    insert() {
        let tests = [];

        try {
            tests = JSON.parse(localStorage.getItem('tests'));
            if (tests === null) {
                tests = []
            }
        } catch (e) {
            localStorage.removeItem('tests');
        }

        let id = 1;
        tests.forEach((test) => {
            id = test.id >= id ? test.id + 1 : id;
        })
        this.id = id;

        tests.push(this);

        localStorage.tests = JSON.stringify(tests);
        return this;
    }

    static getAll() {
        let tests = [];

        if (localStorage.tests) {
            tests = JSON.parse(localStorage.tests).map(function (testInformation) {
                return new Test(testInformation);
            });
        }

        localStorage.tests = JSON.stringify(tests)
        return tests;
    }

    static getById(id) {
        return Test.getAll().filter(test => test.id === id)[0] ?? null;
    };

    static where(column, value) {
        return Test.getAll().filter(test => test[column] === value);
    };

    update() {
        localStorage.tests = JSON.stringify(
            Test
                .getAll()
                .map((test) => {
                    return test.id === this.id
                        ? this
                        : test
                })
        );

        return this;
    };

    static delete(id) {
        let newTest = Test
            .getAll()
            .filter(test => test.id !== id)

        localStorage.tests = JSON.stringify(newTest);

        return newTest
    };
}