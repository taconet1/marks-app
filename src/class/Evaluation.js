export default class Evaluation {
    name = "";
    id = null;
    classId = null;
    testId = null;
    done = false;

    constructor(value) {
        this.name = value.name;
        this.id = value.id ?? null;
        this.classId = value.classId ?? null;
        this.testId = value.testId ?? null;
        this.done = value.done ?? false
    }

    insert() {
        let evaluations = [];

        try {
            evaluations = JSON.parse(localStorage.getItem('evaluations'));
            if (evaluations === null) {
                evaluations = []
            }
        } catch (e) {
            localStorage.removeItem('evaluations');
        }

        let id = 1;
        evaluations.forEach((evaluation) => {
            id = evaluation.id >= id ? evaluation.id + 1 : id;
        })
        this.id = id;
        evaluations.push(this);

        localStorage.evaluations = JSON.stringify(evaluations);
        return this;
    }

    static getAll() {
        let evaluations = [];

        if (localStorage.evaluations) {
            evaluations = JSON.parse(localStorage.evaluations).map(function (evaluationInformation) {
                return new Evaluation(evaluationInformation);
            });
        }

        localStorage.evaluations = JSON.stringify(evaluations)
        return evaluations;
    }

    static getById(id) {
        return Evaluation.getAll().filter(evaluation => evaluation.id === id)[0] ?? null;
    };

    update() {
        localStorage.evaluations = JSON.stringify(
            Evaluation
                .getAll()
                .map((evaluation) => {
                    return evaluation.id === this.id
                        ? this
                        : evaluation
                })
        );
    };

    static delete(id) {
        let newEvaluations = Evaluation
            .getAll()
            .filter(evaluation => evaluation.id !== id);

        localStorage.evaluations = JSON.stringify(newEvaluations);

        return newEvaluations;
    };
}