export default class Class {
    name = "";
    id = null;

    constructor(value) {
        this.name = value.name;
        this.id = value.id ?? null;
    }

    insert() {
        let classes = [];

        try {
            classes = JSON.parse(localStorage.getItem('classes'));
            if (classes === null) {
                classes = []
            }
        } catch (e) {
            localStorage.removeItem('classes');
        }

        let id = 1;
        classes.forEach((list) => {
            id = list.id >= id ? list.id + 1 : id;
        })
        this.id = id;

        classes.push(this);

        localStorage.classes = JSON.stringify(classes);
        return this;
    }

    static getAll() {
        let classes = [];

        if (localStorage.classes) {
            classes = JSON.parse(localStorage.classes).map(function (listInformation) {
                return new Class(listInformation);
            });
        }

        localStorage.classes = JSON.stringify(classes)
        return classes;
    }

    static getById(id) {
        return Class.getAll().filter(list => list.id === id)[0] ?? null;
    };

    update() {
        localStorage.classes = JSON.stringify(
            Class
                .getAll()
                .map((list) => {
                    return list.id === this.id
                        ? this
                        : list
                })
        );
    };

    static delete(id) {
        let newClass = Class
            .getAll()
            .filter(list => list.id !== id)
        localStorage.classes = JSON.stringify(newClass)

        return newClass
    };
}