export default class User {
    lastname = "";
    firstname = "";
    id = null;
    classId = null

    constructor(value) {
        this.lastname = value.lastname;
        this.firstname = value.firstname;
        this.id = value.id ?? null;
        this.classId = value.classId ?? null;
    }

    insert() {
        let users = [];
        try {
            users = JSON.parse(localStorage.getItem('users'));
            if (users === null) {
                users = []
            }
        } catch (e) {
            localStorage.removeItem('users');
        }

        let id = 1;
        users.forEach((user) => {
            id = user.id >= id ? user.id + 1 : id;
        })
        this.id = id;

        users.push(this);

        localStorage.users = JSON.stringify(users);
        return this;
    }

    static getAll() {
        let users = [];

        if (localStorage.users) {
            users = JSON.parse(localStorage.users).map(function (userInformation) {
                return new User(userInformation);
            });
        }

        localStorage.users = JSON.stringify(users)
        return users;
    }

    static getById(id) {
        return User.getAll().filter(user => user.id === id)[0] ?? null;
    };

    static where(column, value) {
        return User.getAll().filter(user => user[column] === value);
    };

    update() {
        localStorage.users = JSON.stringify(
            User
                .getAll()
                .map((user) => {
                    return user.id === this.id
                        ? this
                        : user
                })
        );
    };

    delete() {
        localStorage.users = JSON.stringify(
            User
                .getAll()
                .filter(user => user.id !== this.id)
        );
    };

    static deleteAllWhere(column, value) {
        localStorage.users = JSON.stringify(
            User
                .getAll()
                .filter(user => user[column] !== value)
        );
    }
}