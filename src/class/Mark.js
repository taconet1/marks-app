export default class Mark {
    mark = "";
    userId = null;
    criterionId = null;
    evaluationId = null;

    constructor(value) {
        this.mark = value.mark;
        this.userId = parseInt(value.userId) ?? null;
        this.criterionId = parseInt(value.criterionId) ?? null;
        this.evaluationId = parseInt(value.evaluationId) ?? null;
    }

    insert() {
        let marks = [];

        try {
            marks = JSON.parse(localStorage.getItem('marks'));
            if (marks === null) {
                marks = []
            }
        } catch (e) {
            localStorage.removeItem('marks');
        }

        if (Mark.getById(this.userId, this.criterionId, this.evaluationId) !== null) {
            return;
        }

        marks.push(this);

        localStorage.marks = JSON.stringify(marks);
        return this;
    }

    static getAll() {
        let marks = [];

        if (localStorage.marks) {
            marks = JSON.parse(localStorage.marks).map(function (markInformation) {
                return new Mark(markInformation);
            });
        }

        localStorage.marks = JSON.stringify(marks)
        return marks;
    }

    static getById(userId, criterionId, evaluationId) {
        return Mark.getAll()
            .filter(mark => mark.userId === parseInt(userId) && mark.evaluationId === parseInt(evaluationId) && mark.criterionId === parseInt(criterionId))[0] ?? null;
    };

    update() {
        localStorage.marks = JSON.stringify(
            Mark
                .getAll()
                .map((mark) => {
                    return mark.criterionId === this.criterionId && mark.userId === this.userId
                        ? this
                        : mark
                })
        );
    };

    static deleteById(column, id) {
        let newMarks = Mark.getAll().filter(mark => mark[column] !== id)
        localStorage.marks = JSON.stringify(newMarks);
    }
}