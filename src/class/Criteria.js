export default class Criteria {
    name = "";
    id = null;
    testId = null;

    constructor(value) {
        this.name = value.name;
        this.id = value.id ?? null;
        this.testId = value.testId ?? null;
    }

    insert() {
        let criteria = [];

        try {
            criteria = JSON.parse(localStorage.getItem('criteria'));
            if (criteria === null) {
                criteria = []
            }
        } catch (e) {
            localStorage.removeItem('criteria');
        }

        let id = 1;
        criteria.forEach((criterion) => {
            id = criterion.id >= id ? criterion.id + 1 : id;
        })
        this.id = id;

        criteria.push(this);

        localStorage.criteria = JSON.stringify(criteria);
        return this;
    }

    static getAll() {
        let criteria = [];

        if (localStorage.criteria) {
            criteria = JSON.parse(localStorage.criteria).map(function (criterionInformation) {
                return new Criteria(criterionInformation);
            });
        }

        localStorage.criteria = JSON.stringify(criteria)
        return criteria;
    }

    static getById(id) {
        return Criteria.getAll().filter(criterion => criterion.id === id)[0] ?? null;
    };

    static where(column, value) {
        return Criteria.getAll().filter(criterion => criterion[column] === value);
    };

    update() {
        localStorage.criteria = JSON.stringify(
            Criteria
                .getAll()
                .map((criterion) => {
                    return criterion.id === this.id
                        ? this
                        : criterion
                })
        );
    };

    static delete(id) {
        let newCriterion = Criteria
            .getAll()
            .filter(criterion => criterion.id !== id)

        localStorage.criteria = JSON.stringify(newCriterion);

        return newCriterion
    };

    static deleteAllWhere(column, value) {
        localStorage.criteria = JSON.stringify(
            Criteria
                .getAll()
                .filter(criterion => criterion[column] !== value)
        );
    }
}